# Bible API tester
This is sample demo API test framework for [Bible API](https://scripture.api.bible/)

## Summary
I wanted to present a solution to this task that would be simple, easy to create, maintain and preview.
Also, I include Swagger specification of Bible API, Based on it's published [swagger](https://api.scripture.api.bible/v1/swagger.json) file, framework use Swagger Codegen Maven Plugin to generate POJO classes to interact with and API

I have never worked with Serenity before, but seeing already it has great capabilities and is so versatile.
I have added 4 Scenarios into 'check_list_of_bibles.feature', where one is expected to fail ('Unable to retrieve list of bibles using invalid API access key'), for test failure demonstration purposes.

## Running tests
### Running with Maven
- run command ```mvn clean verify```
- after test execution is finished, see the report in ```target/site/serenity/index.html```

### Running locally with IntelliJ
- run Cucumber feature file (or Scenario inside)
- after test execution is finished, see the report in ```target/site/serenity/index.html```

## Adding new test
###1. Add scenario
Depending on your task, to add a new scenario(s), either update exiting feature file or create new one in:
```
src/test/resources/features
```

###2. Add glue code
This will link Gherkin expressions into methods calling Steps

example:
```me.leaseplan.stepdefinitions.ListBiblesStepDefinitions```
```
    @Steps
    BiblesListSteps bibles;

    @Given("correct API access key is used")
    public void correctAPIAccessKeyIsUsed() {
        bibles.setAccessKey();
    }
```

###3. Implement steps
This is actual implementation of action being made, such as: a setup, test action, or assertion/verification
example:
```me.leaseplan.stepdefinitions.bibles.BiblesListSteps```
```
    @Step("API access key is used")
    public void setAccessKey() {
        biblesApi.getApiClient().addDefaultHeader("api-key", API_KEY);
    }
```

###4. Confirm your test works in IDE (like IntelliJ)
Right-click on newly added scenario (or feature file) and "Run"

###5. Confirm your test works in Maven build
- run command ```mvn clean verify```
- after test execution is finished, see the report in ```target/site/serenity/index.html``` and make sure your test worked as epxected

###6. Integrate with master branch
- push your changes
- create Pull Request / Merge Request
- let your team take a look and approve
- merge to master