package me.leaseplan.steps;

import bible.api.ApiException;
import bible.api.BiblesApi;
import bible.api.model.InlineResponse200;
import me.leaseplan.BibleApiClient;

public abstract class BaseStep {

    public static final String API_KEY = "3c8c1e5901fce07ed267f98a098a1c04";

    protected static BibleApiClient bibleClient;
    protected static  BiblesApi biblesApi;

    protected InlineResponse200 response200;
    protected ApiException errorResponse;

    static {
        bibleClient = new BibleApiClient();
        biblesApi = new BiblesApi(bibleClient.getApiClient());
    }

}
