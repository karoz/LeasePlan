package me.leaseplan.steps;

import bible.api.ApiException;
import net.thucydides.core.annotations.Step;
import org.assertj.core.api.SoftAssertions;

import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class BiblesListSteps extends BaseStep {

    @Step("API access key is used")
    public void setAccessKey() {
        biblesApi.getApiClient().addDefaultHeader("api-key", API_KEY);
    }

    @Step("Invalid API access key is used")
    public void setInvalidAccessKey() {
        biblesApi.getApiClient().addDefaultHeader("api-key", UUID.randomUUID().toString());
    }

    @Step("I request list of all bibles")
    public void retrieveAllBibles() {
        try {
            response200 = biblesApi.getBibles(null, null, null, null, true);
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    @Step("I request list of all bibles expecting failure")
    public void retrieveAllBiblesExpectingFailure() {
        try {
            biblesApi.getBibles(null, null, null, null, true);
        } catch(ApiException e) {
            errorResponse = e;
        }
    }

    @Step("the API should return an error {int}")
    public void assertErrorResponse(int code) {
        assertThat(errorResponse.getCode()).isEqualTo(code);
    }

    @Step("I request list of bibles for language code: {word}")
    public void retrieveBiblesForLanguageCode(String languageISO639_3) {
        try {
            response200 = biblesApi.getBibles(languageISO639_3, null, null, null, true);
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    @Step("I request list of bibles for language: {word}")
    public void assertResponseNotEmpty() {
        SoftAssertions softAssertions = new SoftAssertions();

        softAssertions.assertThat(response200.getData().size()).isGreaterThan(0);
        response200.getData().forEach(bibleSummary ->
                softAssertions.assertThat(bibleSummary.getCountries()).isNotNull()
        );

        softAssertions.assertAll();
    }

    @Step("the API should return list of bibles only in {word} with local name {word}")
    public void assertResponseLanguagesOnly(String language, String localName) {
        SoftAssertions softAssertions = new SoftAssertions();

        softAssertions.assertThat(response200.getData().size()).isGreaterThan(0);
        response200.getData().forEach(bibleSummary ->
                softAssertions.assertThat(bibleSummary.getLanguage().getName()).isEqualTo(language)
        );
        response200.getData().forEach(bibleSummary ->
                softAssertions.assertThat(bibleSummary.getLanguage().getNameLocal()).isEqualTo(localName)
        );

        softAssertions.assertAll();
    }

}
