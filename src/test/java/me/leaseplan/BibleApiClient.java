package me.leaseplan;

import bible.api.ApiClient;
import com.squareup.okhttp.*;
import lombok.Getter;
import okio.Buffer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import static java.lang.System.*;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class BibleApiClient {

    final String DEFAULT_TIMEOUT = "180000"; // ms

    @Getter
    private final ApiClient apiClient;

    public BibleApiClient() {
        int maxIdleConnections = new Integer(getProperty("bible.api.client.maxIdleConnections", "100"));
        int connectionTimeout =  new Integer(getProperty("bible.api.client.connectionTimeout", DEFAULT_TIMEOUT));
        long readTimeout = new Long(getProperty("bible.api.client.readTimeout", DEFAULT_TIMEOUT));
        long writeTimeout = new Long(getProperty("bible.api.client.writeTimeout", DEFAULT_TIMEOUT));
        long keepAliveDuration = new Long(getProperty("bible.api.client.keepAliveDuration", DEFAULT_TIMEOUT));
        ConnectionPool connectionPool = new ConnectionPool(maxIdleConnections, keepAliveDuration);

        ApiClient client = new ApiClient();
        client.setBasePath("https://api.scripture.api.bible");
        client.addDefaultHeader("User-Agent", "SerenityTest-$" + currentTimeMillis());
        client.setConnectTimeout(connectionTimeout);
        client.getHttpClient().setConnectionPool(connectionPool);
        client.getHttpClient().setReadTimeout(readTimeout, MILLISECONDS);
        client.getHttpClient().setWriteTimeout(writeTimeout, MILLISECONDS);
        client.getHttpClient().interceptors().add(logMonitor);

        System.out.printf("%s client ready - endpoint: %s%n",
                client.getClass().getPackage().getName(), client.getBasePath());
        apiClient = client;
    }

    Interceptor logMonitor = chain -> {
        Request request = chain.request();
        RequestBody requestBody = request.newBuilder().build().body();
        StringBuilder requestLines = new StringBuilder();
        if (requestBody != null) {
            Buffer buffer = new Buffer();
            requestBody.writeTo(buffer);
            BufferedReader reader = new BufferedReader(new InputStreamReader(buffer.inputStream(), StandardCharsets.UTF_8));
            while(reader.ready()) {
                requestLines.append(reader.readLine()).append("\n");
            }
        }
        System.out.printf(">> [%d] %s %s : %s%n", getThreadId(), request.method(), request.url(), requestLines);
        if (getProperty("bible.api.client.printRequestHeaders", "true").equalsIgnoreCase("true")) {
            System.out.println(request.headers());
        }

        final long startTime = nanoTime();
        Response response = chain.proceed(request);
        final long endTime = nanoTime();

        MediaType contentType = null;
        String bodyString = "";
        if (response.body() != null) {
            contentType = response.body().contentType();
            bodyString = response.body().string();
        }
        System.out.printf("<< [%d] %s : %s [%d] in %f ms : %s%n",
                getThreadId(), request.method(), request.url(), response.code(), ((endTime - startTime)/1e6d), bodyString);
        if (getProperty("bible.api.client.printResponseHeaders", "true").equalsIgnoreCase("true")) {
            System.out.println(response.headers());
        }

        ResponseBody responseBody = ResponseBody.create(contentType, bodyString);
        response = response.newBuilder().body(responseBody).build();
        return response;
    };

    static long getThreadId() {
        return Thread.currentThread().getId();
    }

}
