package me.leaseplan.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import me.leaseplan.steps.BiblesListSteps;
import net.thucydides.core.annotations.Steps;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class ListBiblesStepDefinitions {

    @Steps
    BiblesListSteps bibles;

    @Given("correct API access key is used")
    public void correctAPIAccessKeyIsUsed() {
        bibles.setAccessKey();
    }

    @Given("invalid API access key is used")
    public void invalidAPIAccessKeyIsUsed() {
        bibles.setInvalidAccessKey();
    }

    @When("I request list of all bibles")
    public void iRequestListOfAllBibles() {
        bibles.retrieveAllBibles();
    }

    @When("I request list of all bibles expecting failure")
    public void iRequestListOfAllBiblesExpectingFailure() {
        bibles.retrieveAllBiblesExpectingFailure();
    }

    @When("I request list of bibles for language code: {word}")
    public void iRequestListOfBiblesForLanguageCode(String LanguageISO639_3) {
        bibles.retrieveBiblesForLanguageCode(LanguageISO639_3);
    }

    @Then("the API should return an error {int}")
    public void theAPIShouldReturnAnError(int code) {
        bibles.assertErrorResponse(code);
    }

    @Then("the API should return list of all bibles")
    public void theAPIShouldReturnListOfAllBibles() {
        bibles.assertResponseNotEmpty();
    }

    @Then("the API should return list of bibles only in {word} with local name {word}")
    public void theAPIShouldReturnListOfBiblesOnlyIn(String language, String localName) {
        bibles.assertResponseLanguagesOnly(language, localName);
    }

}
