Feature: Check list of bibles

  The list of bibles returns information about bibles

  Scenario: Unable to retrieve list of bibles using invalid API access key
    Given invalid API access key is used
    When I request list of all bibles expecting failure
    Then the API should return an error 401

  Scenario: List of all bibles
    Given correct API access key is used
    When I request list of all bibles
    Then the API should return list of all bibles

  Scenario Outline: Code <LanguageISO639_3> returns only bibles in <Language>
    Given correct API access key is used
    When I request list of bibles for language code: <LanguageISO639_3>
    Then the API should return list of bibles only in <Language> with local name <LocalName>
    Examples:
      | LanguageISO639_3 | Language   | LocalName |
      | eng              | English    | English   |
      | pol              | Polish     | Polski    |
      | ruf              | Chilughuru | Luguru    |
      | bla              | Siksika    | Siksika   |